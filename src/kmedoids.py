#A template for the implementation of K-Medoids.
import math #sqrt
import random
from graphics import *

#our helper functions
import spreads

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
# Note: This can be identical to the K-Means function of the same name.
def assignClusters(D, centers):
    dic = {c:[] for c in centers}
    for d in D:
        center = None
        for c in centers:
            if center == None:
                center = c
            else:
                if spreads.manhattanDistance(d,c) < spreads.manhattanDistance(d,center):
                    center = c
        dic[center].append(d)
    return dic
            

# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Medoids clustering
#  of D.
def KMedoids(D, k, win = None):
    centers = D[0:k]
    while True:
        oldCenters = centers[:]
        clusters = assignClusters(D, centers)
        for i,c in enumerate(centers):
            
            try:
                Circle(Point(*c), .005).draw(win).setFill("blue")
            except:
                placeholder = 0
                
        #finding nearest centers
            centers[i] = spreads.findDataMedoid(clusters[c])
        #checking for exit condition
        if centers == oldCenters:
            return clusters		


def graph(D, clust, win):
    for d in D:
        p = Point(d[0], d[1])
        Circle(p, .0025).draw(win)

    vals = KMedoids(D, clust, win = win)
    for i, k in enumerate(vals):
        color = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
        for point in vals[k]:
            Circle(Point(*point), .005).draw(win).setFill(color_rgb(*color))
        Circle(Point(*k), .01).draw(win).setFill(color_rgb(255,0,0))


