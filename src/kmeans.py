#A template for the implementation of K-Means.
import math #sqrt
import random
from graphics import *

#our helper functions
import spreads

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
def assignClusters(D, centers):
    dic = {c:[] for c in centers}
    for d in D:
        center = None
        for c in centers:
            if center == None:
                center = c
            else:
                if spreads.euclidianDistance(d,c) < spreads.euclidianDistance(d,center):
                    center = c
        dic[center].append(d)
            
# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Means clustering
#  of D.
def KMeans(D, k, win = None):
    means = D[0:k]
    while True:
        oldMeans = means[:]
        for k in oldMeans:
            try:
                Circle(Point(*k), .005).draw(win).setFill("blue")
            except:
                placeholder = 0
        clusters = {}
        #finding nearest center
        for d in D:
            nearest = means[0]
            for m in means:
                if spreads.euclidianDistance(d,m) < spreads.euclidianDistance(nearest, d):
                    nearest = m
            #this may freak out
            try:
                clusters[nearest].append(d)
            except:
                clusters[nearest] = [d]
        #get new means
        for i in range(len(means)):
            means[i] = spreads.findDataMean(clusters[means[i]])
        #checking for exit condition
        if means == oldMeans:
            return clusters

D = [(.5,.5)]
def graph(D, clust, win):
    for d in D:
        p = Point(d[0], d[1])
        Circle(p, .0025).draw(win)

    vals = KMeans(D, clust, win = win)
    for i, k in enumerate(vals):
        color = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
        for point in vals[k]:
            Circle(Point(*point), .005).draw(win).setFill(color_rgb(*color))
        Circle(Point(*k), .01).draw(win).setFill(color_rgb(255,0,0))




