import csv
import copy
import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from graphics import *

import kmeans
import kmedoids
import hclust
import spreads

scale = 500
win = GraphWin('', scale + 10,scale + 10)
win.setCoords(0,0,1,1)

file = 'C:\\Users\\Jake From State Farm\\Documents\\GitHub\\dm_a2\\data\\project2data.csv'


data = None

with open('C:/Users/Jake From State Farm/Documents/GitHub/dm_a2/data/project2data.csv', 'r') as f:
    reader = csv.reader(f)
    data = list(reader)

#cleaning data
for x in data:
    try:
        x = list(map(float, x))
    except:
        data.remove(x)
data = [tuple(map(float, x[1:])) for x in data]
test_data = [x[3:5] for x in data]
pca = PCA(n_components = 2)
principleComponents = pca.fit_transform(data)
principleComponents = np.interp(principleComponents, (principleComponents.min(), principleComponents.max()), (0,1))
principleComponents = principleComponents.tolist()
principleComponents = [tuple(map(float, x)) for x in principleComponents]


tree_data = hclust.dataToTrees(principleComponents, win)
kmeans.graph(principleComponents, 20, win)
#kmedoids.graph(principleComponents, 20, win)
#hclustgraph(tree_data, win)

