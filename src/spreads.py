#a file of helper functions for our three implementations
#as well as measures of spread


import math #sqrt
import random
from graphics import *

#defining distances here
#so we can use these in different files and functions later
def manhattanDistance(a,b):
    #returning manhattan distance
    return (sum([abs(i - j) for i,j in zip(a,b)]))

def euclidianDistance(a,b):
    return math.sqrt(sum([(i - j)**2 for i,j in zip(a,b)]))

#defining cluster mean here
#so we can use it in k-means
#and spread measures
#accepts a list of data points and returns the mean
def findDataMean(cluster):
    total = None
    for datapoint in cluster:
        if total == None:
            total = list(datapoint)
        else:
            for j in range(len(datapoint)):
                total[j] += datapoint[j]
     #return scaled sum           
    return tuple([i / len(cluster) for i in total])


#defining cluster medoid here
#so we can use it in k-medoids
#and spread measures
#accepts a list of data points and returns the medoid
def findDataMedoid(cluster):
    #assigning an arbitrary medoid
    medoid = cluster[0]
    
    #checking if any other point is a better fit
    for c in cluster:
        medoid_dist = 0
        cand_dist = 0
        for cand in cluster:
            medoid_dist += manhattanDistance(medoid, cand)
            cand_dist += manhattanDistance(c, cand)
        if cand_dist < medoid_dist:
            medoid = c

    return medoid


#takes a dictionary of clusters with centers and a distance function
#of the form {center:[p0,p1,...pn]}
#returns the intra-cluster spread of the data
def intra(clusters, distanceFunct):
    total = 0
        #getting one center
    for center in clusters:
            #getting the cluster associated with the center
        for p in clusters[center]:
            #adding distance squared so points far away have very high values
            total += (distanceFunct(p, center))**2
    return total

#will also take in a dictionary of clusters and distance
#but we only care about the centers here
def inter(clusters, centerFunct, distFunct):
    center = centerFunct([c for c in clusters])
    total = 0
    for c in clusters:
        total += distFunct(c, center)**2
    return total/len(clusters)











    
